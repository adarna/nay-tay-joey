﻿using UnityEngine;
using System.Collections;

public class InputManager : MonoBehaviour {

    private static InputManager _instance;
    public static InputManager _Instance {
        get {
            if(_instance == null) {
                _instance = FindObjectOfType<InputManager>();
            }
            return _instance;
        }
    }

    void Start() {
        if(FindObjectsOfType<InputManager>().Length > 1) {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    //public bool noInput;
    public InputMode mode = InputMode.General;

    public enum InputMode {
        UI, Interaction, Reaction, noInput, General
    }
}
