﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class NextButton : MonoBehaviour, IPointerClickHandler {

    private static NextButton _instance;
    public static NextButton _Instance {
        get {
            if(_instance == null) {
                _instance = FindObjectOfType<NextButton>();
            }
            return _instance;
        }
    }

    public GameObject sprites;
    private bool enabled = false;
    private bool pressed = false;

    void Start() {
        sprites.SetActive(false);
    }

    public void OnPointerClick(PointerEventData eventData) {
        if(enabled && !pressed) {
            ScenePageManager._Instance.NextScene();
            pressed = true;
        }
    }

    public void EnableNext() {
        //StartCoroutine(FadeIn());
        sprites.SetActive(true);
        enabled = true;
    }

    //private IEnumerator FadeIn() {
    //    while(sprite.color.a < 1) {
    //        foreach(SpriteRenderer s in sprites) {
    //            Color c = s.color;
    //            c.a += 0.1f;
    //            s.color = c;
    //        }

    //        yield return new WaitForSeconds(0.1f);
    //    }
    //    enabled = true;
    //}
}
