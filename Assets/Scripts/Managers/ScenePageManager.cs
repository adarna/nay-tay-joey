﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class ScenePageManager : MonoBehaviour {

    private static ScenePageManager _instance;
    public static ScenePageManager _Instance {
        get {
            if(_instance == null) {
                _instance = FindObjectOfType<ScenePageManager>();
            }
            return _instance;
        }
    }

    public delegate void PageEnter();
    public delegate void PageExit();

    public PageEnter OnPageEnter;
    public PageExit OnPageExit;

    //public GameObject NextButton;

    public Animator pageDirector;
    public Animator transitionAnim;

    public Camera mainCamera;

    public int page { get; private set; }

    public enum DirectorSignal {
        EntranceEnd, ReactionEnd, InteractionEnd
    }

    void Start() {
        page = 0;
    }

    public void LoadLevel(string s) {
        OnPageExit();
        SceneManager.LoadScene(s);
        StartCoroutine(DelayEnter());
    }

    public IEnumerator DelayEnter() {
        yield return new WaitForSeconds(0.001f);
        OnPageEnter();
    }

    public void NextScene() {
        //OnPageExit();
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        StartCoroutine(DelaySceneLoad(SceneManager.GetActiveScene().buildIndex + 1));
        pageDirector.SetTrigger("Next Page");
        transitionAnim.SetTrigger("Close");
        EnableNextButton(false);
    }

    private IEnumerator DelaySceneLoad(int i) {
        yield return new WaitForSeconds(2.5f);
        SceneManager.LoadScene(i);
    }

    public void EnableNextButton(bool show = true) {
        //NextButton.SetActive(show);
        Debug.Log("Calling next button: " + NextButton._Instance.gameObject);
        NextButton._Instance.EnableNext();
    }

    public void SignalDirector(DirectorSignal signal) {
        if(signal == ScenePageManager.DirectorSignal.EntranceEnd) {
            pageDirector.SetTrigger("EntranceEnd");
        }
        else if(signal == ScenePageManager.DirectorSignal.ReactionEnd) {
            pageDirector.SetTrigger("ReactionEnd");
        }
    }

    private const string PAGE16BY9EXTENTION = "_2";
    public void LoadPage(int p) {
        string pagename = string.Format("sperad{0:00}", p);
        //Vector2 aspectratio = AspectRatio.GetAspectRatio(Screen.width, Screen.height, true);
        //if(Screen.height)
        if(Camera.main.aspect >= 1.7) {
            pagename += PAGE16BY9EXTENTION;
        }

        //OnPageExit();
        SceneManager.LoadScene(pagename);
        StartCoroutine(DelayEnter());

        page = p;
    }

    public void SwitchToCamera() {
        mainCamera.gameObject.SetActive(true);
    }

    public void NextPage() {
        LoadPage(++page);
    }
}
