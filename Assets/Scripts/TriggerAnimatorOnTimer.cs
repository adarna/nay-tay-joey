﻿using UnityEngine;
using System.Collections;

// Requires an Animator if target is not set.
// Requires a "Visible" bool parameter on the Animator if trigger is not set.
public class TriggerAnimatorOnTimer : MonoBehaviour {
    const string PARAMETER_NAME = "Timer";
    
    public Animator target;
    public string trigger;
    public float seconds;
    public float secondsMin;
    public float secondsMax;
    
    void Start() {
        if (target == null) {
            target = GetComponent<Animator>();
        }
        if (string.IsNullOrEmpty(trigger)) {
            trigger = PARAMETER_NAME;
        }

        StartCoroutine(Countdown());
    }

    IEnumerator Countdown() {
        while (seconds > 0) {
            yield return new WaitForSeconds(seconds);
            target.SetTrigger("Timer");
            seconds = Random.Range(secondsMin, secondsMax);
        }
    }
}
