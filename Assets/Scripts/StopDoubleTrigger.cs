﻿using UnityEngine;
using System.Collections;

public class StopDoubleTrigger : StateMachineBehaviour {

    public string trigger;
    private const string DEFAULTTRIGGER = "Tap";

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if(string.IsNullOrEmpty(trigger))
            trigger = DEFAULTTRIGGER;
        animator.ResetTrigger(trigger);
    }
}
