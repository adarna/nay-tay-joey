﻿using UnityEngine;
using System.Collections;

public class AnimationEventController : MonoBehaviour {

	public Animator[] animators;
	
	public void OnAnimationEvent(AnimationEvent animEvent) {
		int intParam = animEvent.intParameter;
		string strParam = animEvent.stringParameter;

        //Debug.Log(this.gameObject.name + " triggers " + animators[intParam].name);

		animators[intParam].SetTrigger(strParam);
	}
}
