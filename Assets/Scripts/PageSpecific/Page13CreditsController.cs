﻿using UnityEngine;
using System.Collections;

public class Page13CreditsController : MonoBehaviour {

    public Animator credits;
    private int talks;

    public Animator boyAnim;
    public Animator girlAnim;

    public void CheckAnims() {
        Debug.Log(boyAnim.GetBool("Active"));
        Debug.Log(girlAnim.GetBool("Active"));

        if(boyAnim.GetBool("Active") && girlAnim.GetBool("Active")) {
            credits.SetTrigger("Ending");
        }
    }
}
