﻿using UnityEngine;
using System.Collections;

public class Page4ResetStateMachine : StateMachineBehaviour {

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        base.OnStateExit(animator, stateInfo, layerIndex);

        Page4Controller._Instance.ResetPage();
    }
}
