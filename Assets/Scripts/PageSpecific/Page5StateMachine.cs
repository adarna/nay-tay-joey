﻿using UnityEngine;
using System.Collections;

public class Page5StateMachine : StateMachineBehaviour {

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        base.OnStateExit(animator, stateInfo, layerIndex);

        Page5SignalAnimator signal = animator.GetComponent<Page5SignalAnimator>();

        signal.Trigger();

        Page5Controller._Instance.ChangeBackground(true);

        ScenePageManager._Instance.EnableNextButton();
    }
}
