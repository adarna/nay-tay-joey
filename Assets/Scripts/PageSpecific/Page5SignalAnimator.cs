﻿using UnityEngine;
using System.Collections;

public class Page5SignalAnimator : MonoBehaviour {

    public Animator targetAnimator;
    public string triggerName;

    //For Page5StateMachine
    public void Trigger() {
        targetAnimator.SetTrigger(triggerName);
    }
}
