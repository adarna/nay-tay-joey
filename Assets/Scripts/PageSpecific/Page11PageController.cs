﻿using UnityEngine;
using System.Collections;

public class Page11PageController : MonoBehaviour {

    private static Page11PageController _instance;
    public static Page11PageController _Instance {
        get {
            if(_instance == null) {
                _instance = FindObjectOfType<Page11PageController>();
            }
            return _instance;
        }
    }

    public Animator nakuAnim;
    public Animator momAnim;
    public Animator girlAAnim;
    public Animator girlBAnim;

    public Page11PictureHandler pictureHandler;
    public Page11GirlDrag girlController;

    private bool broken = false;

    public void Break() {
        nakuAnim.SetBool("Appear", true);
        nakuAnim.SetTrigger("Break");
        momAnim.SetBool("Talk", true);
        girlAAnim.SetTrigger("Exit");
        girlBAnim.SetBool("Show", true);

        broken = true;
    }

    public void ResetPage() {
        if(!broken)
            return;

        nakuAnim.SetBool("Appear", false);
        nakuAnim.SetTrigger("Fix");
        girlAAnim.SetTrigger("Return");
        girlBAnim.SetBool("Show", false);
        momAnim.SetBool("Talk", false);

        pictureHandler.GetComponent<Animator>().SetTrigger("Fix");
        pictureHandler.Reset();

        girlController.transform.position = Vector3.zero;

        ScenePageManager._Instance.EnableNextButton();
    }
}
