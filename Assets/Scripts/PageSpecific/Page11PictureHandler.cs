﻿using UnityEngine;
using System.Collections;

public class Page11PictureHandler : MonoBehaviour {

    public Animator pictureAnim;
    public string shakeTriggerName;
    public string breakIntName;

    void OnTriggerEnter2D(Collider2D other) {
        pictureAnim.SetTrigger(shakeTriggerName);
        pictureAnim.SetInteger(breakIntName, pictureAnim.GetInteger(breakIntName) + 1);
    }

    public void Reset() {
        pictureAnim.SetInteger(breakIntName, 0);
    }
}
