﻿using UnityEngine;
using System.Collections;

public class Page9BoyController : MonoBehaviour {

    public Page9BoyDrop[] boys;
    public Animator boyEwww;

    public Page9Clothes[] clothes;
    public Animator nayWash;

    public void ResetCloth() {
        foreach(Page9BoyDrop b in boys) {
            b.Reset();
        }

        foreach(Page9Clothes c in clothes) {
            c.ResetCloth();
        }

        ScenePageManager._Instance.EnableNextButton();
    }
}
