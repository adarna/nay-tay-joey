﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class Page2Candy : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

    private Collider2D collider;
    private bool candyHold = false;
    private Vector3 startingPosition;

    public Page2Controllers controller;
    public Collider target;

    void Start() {
        startingPosition = transform.position;
        collider = GetComponent<Collider2D>();
    }

    void Update() {
        if(candyHold) {
            transform.position = Camera.main.ScreenToWorldPoint(new Vector3(
                Input.mousePosition.x,
                Input.mousePosition.y,
                Camera.main.nearClipPlane + 10)
            );
        }
    }

    public void OnPointerDown(PointerEventData eventData) {
        candyHold = true;
        GetComponent<Animator>().enabled = false;
    }

    public void OnPointerUp(PointerEventData eventData) {
        candyHold = false;
        transform.position = startingPosition;

        GetComponent<Animator>().enabled = true;

        Ray ray;
        RaycastHit hit;
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        //Debug.DrawRay(ray.origin, ray.direction, Color.red, 1000000f);

        if(Physics.Raycast(ray, out hit)) {
            //Debug.Log(hit.collider);

            if(hit.collider == target) {
                controller.MouthEat(this);
            }
        }
    }
}
