﻿using UnityEngine;
using System.Collections;

public class Page5ChangeChannel : MonoBehaviour {

    public Sprite[] channelSprites;
    public SpriteRenderer renderer;

    private int index = 0;

    public void ChangeChannel() {
        //renderer.sprite = channelSprites[Random.Range(0, channelSprites.Length)];
        renderer.sprite = channelSprites[++index % channelSprites.Length];
    }
}
