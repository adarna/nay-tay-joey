﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class Page2Tatay : MonoBehaviour, IPointerClickHandler {

    public Page2Controllers controller;

    public void OnPointerClick(PointerEventData eventData) {
        if(controller.mouthAray) {
            controller.BrushTeeth();
        }
    }
}
