﻿using UnityEngine;
using System.Collections;

public class Page2CandyAnimatorDisabler : StateMachineBehaviour {

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        base.OnStateExit(animator, stateInfo, layerIndex);
        animator.enabled = false;
    }
}
