﻿using UnityEngine;
using System.Collections;

public class Page11BrokenStateMachine : StateMachineBehaviour {

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        Page11PageController._Instance.Break();
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        base.OnStateExit(animator, stateInfo, layerIndex);
        Page11PageController._Instance.ResetPage();
    }
}
