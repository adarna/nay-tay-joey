﻿using UnityEngine;
using System.Collections;

public class Page3ShipController : MonoBehaviour {

    public Animator shipAnim;
    public Collider shipCollider;
    private int rainCount = 0;

    void OnParticleCollision(GameObject other) {
        //Debug.Log(other);

        if(other.GetComponent<Collider>() == shipCollider) {
            rainCount++;

            shipAnim.SetInteger("RainCount", rainCount);
        }
    }

    public void ResetCount() {
        rainCount = 0;
        shipAnim.SetInteger("RainCount", rainCount);
    }
}
