﻿using UnityEngine;
using System.Collections;

public class Page7ResetPage : StateMachineBehaviour {

    //public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //    base.OnStateExit(animator, stateInfo, layerIndex);

        
    //}

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        Page7ToyController._Instance.ResetPage();
    }
}
