﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class Page2Candy_old : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

    public Page2Controllers controller;

    //public Collider2D collider;
    public Transform candyDummy; //dummy for testing
    public Transform[] candyDummies;
    private bool candyHold = false;
    private Vector3 startingPosition;

    public Collider target;

    void Start() {
        startingPosition = candyDummy.position;
    }

    void Update() {
        if(candyHold) {
            candyDummy.position = Camera.main.ScreenToWorldPoint(new Vector3(
                Input.mousePosition.x,
                Input.mousePosition.y,
                Camera.main.nearClipPlane + 10)
            );
        }
    }

    public void OnPointerDown(PointerEventData eventData) {
        candyHold = true;
        RandomCandy();
    }

    public void OnPointerUp(PointerEventData eventData) {
        candyHold = false;
        candyDummy.position = startingPosition;

        Ray ray;
        RaycastHit hit;
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        //Debug.DrawRay(ray.origin, ray.direction, Color.red, 1000000f);

        if(Physics.Raycast(ray, out hit)) {
            //Debug.Log(hit.collider);

            if(hit.collider == target) {
                //controller.MouthEat();
            }
        }
    }

    public void RandomCandy() {
        candyDummy = candyDummies[Random.Range(0, candyDummies.Length)];
    }

    //private IEnumerator FadeInColor(Color color) {
    //    while(color.a >= 1) {
    //        yield return new WaitForSeconds(0.1f);
    //        Color c = color;
    //        c.a -= 0.05f;
    //    }
    //}
}
