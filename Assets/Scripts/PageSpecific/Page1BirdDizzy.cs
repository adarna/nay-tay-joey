﻿using UnityEngine;
using System.Collections;

public class Page1BirdDizzy : MonoBehaviour {

    public Animator dizzyAnim;
    public ParticleSystem dizzyParticle;

    public void BirdDizzy() {
        dizzyAnim.SetTrigger("Dizzy");
        dizzyParticle.Play();
    }
}
