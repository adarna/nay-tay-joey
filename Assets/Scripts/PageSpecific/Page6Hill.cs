﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class Page6Hill : MonoBehaviour, IPointerClickHandler {

    public Animator boyKatiAnim;
    public Animator tayAnim;
    public Animator[] bugAnims;
    private int bugs = 0;
    private const int MAXBUGS = 20;

    public void TriggerRandomBug() {
        if(boyKatiAnim.GetBool("Active"))
            return;

        bugAnims[Random.Range(0, bugAnims.Length)].SetTrigger("Pop");
        bugs++;

        if(bugs >= MAXBUGS) {
            boyKatiAnim.SetTrigger("Show");
            tayAnim.SetTrigger("Show");
        }
    }

    public void OnPointerClick(PointerEventData eventData) {
        TriggerRandomBug();
    }

    public void ResetBug() {
        bugs = 0;
    }
}
