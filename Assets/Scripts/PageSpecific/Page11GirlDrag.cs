﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class Page11GirlDrag : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

    public Transform girlTrans;
    public Animator girlAnim;

    private bool hold = false;
    private Vector3 referencePosition;
    private const float LOWERBUFFER = .005f;

    public void OnPointerUp(PointerEventData eventData) {
        hold = false;
    }

    public void OnPointerDown(PointerEventData eventData) {
        hold = true;
        StartCoroutine(FixedCheck());
        referencePosition = girlTrans.position;
    }

    private IEnumerator FixedCheck() {
        do {
            yield return new WaitForSeconds(0.05f);

            Vector3 temp = new Vector3(
                Input.mousePosition.x,
                Input.mousePosition.y,
                Camera.main.nearClipPlane
                );

            Vector3 temp2 = Camera.main.WorldToScreenPoint(referencePosition);

            Vector2 deltatracking = temp - temp2;
            deltatracking *= 0.008f;

            Debug.DrawLine(temp, temp2);
            //Debug.Log(deltatracking);
            //Debug.Log(deltatracking.sqrMagnitude);
            //debugText.text = ("Vector: " + deltatracking + "\nDistance: " + deltatracking.sqrMagnitude);

            if(Mathf.Abs(deltatracking.x + deltatracking.y) > LOWERBUFFER) {
                //deltatracking *= 0.1f;
                //transformToTrack.position = Vector3.Lerp(transformToTrack.position, temp2, 0.5f);
                transform.position += new Vector3(
                    deltatracking.x,
                    deltatracking.y,
                    0
                );
            }

            if(deltatracking.x > LOWERBUFFER) {
                girlAnim.SetBool("Right", true);
            }
            else if(deltatracking.x < -LOWERBUFFER){
                girlAnim.SetBool("Right", false);
            }

            if(deltatracking.y > LOWERBUFFER) {
                girlAnim.SetBool("Up", true);
            }
            else if(deltatracking.y < -LOWERBUFFER) {
                girlAnim.SetBool("Up", false);
            }

            referencePosition = girlTrans.position;
        } while(hold);
    }
}
