﻿using UnityEngine;
using System.Collections;

public class TittleEntranceCameraDisablerStateMachine : StateMachineBehaviour {

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        ScenePageManager._Instance.SwitchToCamera();
        Destroy(animator.gameObject);
    }
}
