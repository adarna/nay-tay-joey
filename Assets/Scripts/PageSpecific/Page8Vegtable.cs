﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class Page8Vegtable : MonoBehaviour, IPointerClickHandler {

    public Page8PlateController controller;

    public void OnPointerClick(PointerEventData eventData) {
        controller.IncreaseJunk();
    }
}
