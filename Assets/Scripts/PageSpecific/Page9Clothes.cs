﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class Page9Clothes : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

    public SpriteRenderer clothSprite;
    
    public Color startingColor;
    public Color targetColor;
    public float dirty;

    public ClothType type;

    private bool holding;
    private Vector3 startingPoint;

    public enum ClothType {
        Shirt, Shorts
    }

    void Start() {
        dirty = 0;
        startingColor = clothSprite.color;
        holding = false;
        startingPoint = transform.position;
    }

    public void ResetCloth() {
        dirty = 0;
        //clothSprite.color = startingColor;
        StartCoroutine(FadeToStartingColor());
    }

    private IEnumerator FadeToStartingColor() {
        float l = 0;
        Color colortemp = clothSprite.color;

        do {
            yield return new WaitForSeconds(0.05f);
            //Debug.Log(clothSprite.color);
            clothSprite.color = Color.Lerp(colortemp, startingColor, l);
            l += 0.1f;
        } while(clothSprite.color != startingColor);
    }

    public void WearCloth() {
        dirty += 0.25f;
        Color c = Color.Lerp(startingColor, targetColor, dirty);
        clothSprite.color = c;
    }

    public void OnPointerUp(PointerEventData eventData) {
        holding = false;

        //var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        var ray = new Ray2D();
        ray.origin = transform.position;
        ray.direction = Vector3.forward;
        Debug.DrawRay(ray.origin, ray.direction, Color.white, 100000f);
        RaycastHit2D[] raycasthit2d = Physics2D.RaycastAll(ray.origin, ray.direction);

        //Debug.Log(raycasthit2d[0].collider);

        Page9BoyDrop boy = raycasthit2d[0].collider.GetComponent<Page9BoyDrop>();

        if(boy != null) {
            //Debug.Log(raycasthit2d[0].collider.GetComponent<Page9BoyDrop>());
            boy.ClothDrop(this);
        }

        transform.position = startingPoint;
    }

    public void OnPointerDown(PointerEventData eventData) {
        holding = true;
    }

    void Update() {
        if(holding) {
            transform.position = Camera.main.ScreenToWorldPoint(new Vector3(
                Input.mousePosition.x,
                Input.mousePosition.y,
                Camera.main.nearClipPlane + 10)
            );
        }
    }
}
