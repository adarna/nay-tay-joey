﻿using UnityEngine;
using System.Collections;

public class Page9BoyDrop : MonoBehaviour {

    public int index;
    private bool shirtWorn = false;
    private bool shortsWorn = false;

    public Animator boyAnim;
    public Animator ewwAnim;

    public void ClothDrop(Page9Clothes cloth) {
        if(
            (shirtWorn && cloth.type == Page9Clothes.ClothType.Shirt) ||
            (shortsWorn && cloth.type == Page9Clothes.ClothType.Shorts)
            ) {
            ewwAnim.SetTrigger("Eeek");
        }

        if(!shirtWorn) {
            if(cloth.type == Page9Clothes.ClothType.Shirt) {
                shirtWorn = true;
                boyAnim.SetTrigger("Hey");
                cloth.WearCloth();
            }
        }
        else if(!shortsWorn) {
            if(cloth.type == Page9Clothes.ClothType.Shorts) {
                shortsWorn = true;
                boyAnim.SetTrigger("Hey");
                cloth.WearCloth();
            }
        }
    }

    public void Reset() {
        shirtWorn = false;
        shortsWorn = false;
    }
}
