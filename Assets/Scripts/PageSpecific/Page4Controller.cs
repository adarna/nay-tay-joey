﻿using UnityEngine;
using System.Collections;

public class Page4Controller : MonoBehaviour {

    public Animator[] carAnim;
    public bool[] carBools;
    public Animator dadCarAnim;
    public Animator trafficAnim;
    public Animator girlAnim;

    private string showTriggerName = "Show";
    private string raceTriggerName = "Race";

    private static Page4Controller _instance;
    public static Page4Controller _Instance {
        get {
            if(_instance == null) {
                _instance = FindObjectOfType<Page4Controller>();
            }
            return _instance;
        }
    }

    public void CarShow(int i) {
        if(!carBools[i]) {
            carAnim[i].SetTrigger(showTriggerName);
            carBools[i] = true;
        }

        if(carBools[0] && carBools[1] && carBools[2]) {
            dadCarAnim.SetTrigger(showTriggerName);
            trafficAnim.SetTrigger(showTriggerName);
        }
    }

    public void ResetPage() {
        carBools[0] = false;
        carBools[1] = false;
        carBools[2] = false;
        girlAnim.SetBool(showTriggerName, true);
        ScenePageManager._Instance.EnableNextButton();
    }
}
