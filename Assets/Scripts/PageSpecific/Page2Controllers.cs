﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Page2Controllers : MonoBehaviour {

    private int candiesEaten = 0;
    public bool mouthAray { get; private set; }

    public Animator mouthAnim;
    private string mouthEatTriggerName = "Eat";
    private string mouthArayBoolName = "Aray";
    //private string mouthBrushTriggerName = "Brush";

    public SpriteRenderer[] decaySprites;
    private float lerpIndex;
    private const float LERPSTEP = 0.8f;

    public Animator tatayAnim;
    private string tatayAngryTriggerName = "Angry";
    private string tatayResolveTriggerName = "Brushed";

    public Animator brushAnim;
    private string brushTriggerName = "Brush";

    public Animator candiesAnim;
    private List<GameObject> candyObjects;

    void Start() {
        mouthAray = false;

        candyObjects = new List<GameObject>();

        foreach(SpriteRenderer spr in decaySprites) {
            spr.color = new Color(spr.color.r, spr.color.g, spr.color.b, 0);
        }
    }

    public void MouthEat(Page2Candy candy) {
        mouthAnim.SetTrigger(mouthEatTriggerName);
        candiesEaten++;

        if(candiesEaten < 8) {
            candyObjects.Add(candy.gameObject);
            candy.gameObject.SetActive(false);
        }

        if(candiesEaten == 4) {
            StopAllCoroutines();
            StartCoroutine(FadeInDecaysHalf());
        }
        else if(candiesEaten == 7) {
            StopAllCoroutines();
            StartCoroutine(FadeInDecaysFull());
        }

        if(candiesEaten >= 8) {
            mouthAray = true;
            mouthAnim.SetBool(mouthArayBoolName, true);
            mouthAnim.SetTrigger(mouthEatTriggerName);
            tatayAnim.SetTrigger(tatayAngryTriggerName);
        }
    }

    public void BrushTeeth() {
        brushAnim.SetTrigger(brushTriggerName);
        tatayAnim.SetTrigger(tatayResolveTriggerName);
        mouthAray = false;
        candiesEaten = 0;

        StopAllCoroutines();
        StartCoroutine(DelayNoMoreAray());
        StartCoroutine(CleanDecays());

        candiesAnim.enabled = true;
        candiesAnim.SetTrigger("Entrace");
        foreach(GameObject obj in candyObjects) {
            obj.SetActive(true);
        }
        candyObjects.RemoveAll(any => true);
    }

    private IEnumerator DelayNoMoreAray() {
        yield return new WaitForSeconds(2.5f);
        mouthAnim.SetBool(mouthArayBoolName, false);
    }

    private IEnumerator FadeInDecaysHalf() {
        while(lerpIndex <= 0.5f) {
            float f = Mathf.Lerp(0, 1f, lerpIndex);

            lerpIndex += LERPSTEP * Time.deltaTime;

            foreach(SpriteRenderer spr in decaySprites) {
                spr.color = new Color(spr.color.r, spr.color.g, spr.color.b, f);
            }

            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator FadeInDecaysFull() {
        while(lerpIndex <= 1f) {
            float f = Mathf.Lerp(0, 1f, lerpIndex);

            lerpIndex += LERPSTEP * Time.deltaTime;

            foreach(SpriteRenderer spr in decaySprites) {
                spr.color = new Color(spr.color.r, spr.color.g, spr.color.b, f);
            }

            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator CleanDecays() {
        while(lerpIndex > 0) {
            float f = Mathf.Lerp(0, 1, lerpIndex);

            lerpIndex -= LERPSTEP * Time.deltaTime;

            foreach(SpriteRenderer spr in decaySprites) {
                spr.color = new Color(spr.color.r, spr.color.g, spr.color.b, f);
            }

            yield return new WaitForEndOfFrame();
        }
    }
}
