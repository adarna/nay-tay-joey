﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class Page8PlateController : MonoBehaviour, IPointerClickHandler {

    public Animator plateAnimator;
    public Animator sickAnimator;
    public Animator korokoAnimator;
    private int junk;

    public const string JUNKTRIGGERNAME = "Junk";
    public const string SICKTRIGGERNAME = "Drop";
    public const string CURETRIGGERNAME = "Return";

    void Start() {
        junk = 0;
    }

    public void IncreaseJunk() {
        plateAnimator.SetInteger(JUNKTRIGGERNAME, ++junk);

        if(junk > 4) {
            sickAnimator.SetTrigger(SICKTRIGGERNAME);
        }
    }

    public void Reset() {
        if(junk <= 4)
            return;

        junk = 0;
        plateAnimator.SetInteger(JUNKTRIGGERNAME, junk);
        sickAnimator.SetTrigger(CURETRIGGERNAME);
        korokoAnimator.SetTrigger(CURETRIGGERNAME);

        ScenePageManager._Instance.EnableNextButton();
    }

    public void OnPointerClick(PointerEventData eventData) {
        Reset();
    }
}
