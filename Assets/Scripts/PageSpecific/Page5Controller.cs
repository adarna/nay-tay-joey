﻿using UnityEngine;
using System.Collections;

public class Page5Controller : MonoBehaviour {

    private static Page5Controller _instance;
    public static Page5Controller _Instance {
        get {
            if(_instance == null) {
                _instance = FindObjectOfType<Page5Controller>();
            }
            return _instance;
        }
    }

    public Animator bgAnim;

    public void ChangeBackground(bool night) {
        if(night) {
            bgAnim.SetTrigger("Night");
        }
        else {
            bgAnim.SetTrigger("Day");
        }
    }
}
