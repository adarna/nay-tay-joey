﻿using UnityEngine;
using System.Collections;

public class Page7ToyController : MonoBehaviour {

    private static Page7ToyController _instance;
    public static Page7ToyController _Instance {
        get {
            if(_instance == null) {
                _instance = FindObjectOfType<Page7ToyController>();
            }
            return _instance;
        }
    }
    
    public bool[] toysActive { get; private set; }
    public Animator toysAnim;
    public Animator girlAnim;
    public Animator bataAnim;
    public Animator[] sampagita;

    void Start() {
        toysActive = new bool[4];

        for(int i = 0; i < toysActive.Length; i++ ) {
            toysActive[i] = true;
        }
    }

    public void HideToy(int index) {
        toysActive[index] = false;

        if(!toysActive[0] && !toysActive[1] && !toysActive[2] && !toysActive[3]) {
            girlAnim.SetBool("Nag", false);
            bataAnim.SetTrigger("Dungaw");
        }
    }

    public void ResetPage() {
        StartCoroutine(DelayedReset());
    }

    public IEnumerator DelayedReset() {
        yield return new WaitForSeconds(3.0f);

        for(int i = 0; i < toysActive.Length; i++) {
            toysActive[i] = true;
        }

        toysAnim.SetBool("Ball", true);
        toysAnim.SetBool("Boat", true);
        toysAnim.SetBool("Airplane", true);
        toysAnim.SetBool("Truck", true);


        bataAnim.SetTrigger("Out");

        foreach(Animator a in sampagita) {
            a.SetBool("Pop", false);
        }

        ScenePageManager._Instance.EnableNextButton();

        yield return new WaitForSeconds(1);

        girlAnim.SetBool("Nag", true);
    }
}
