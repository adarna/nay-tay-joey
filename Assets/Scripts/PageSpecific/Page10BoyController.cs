﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class Page10BoyController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

    public float jumpForceIndex = 10000f;

    public List<Collider2D> collidersInTriggers;
    public Transform transformToTrack;

    private bool hold = false;
    private Vector2 referencePosition;
    private const float LOWERBUFFER = 75f;
    private const float MOVEINDEX = 0.08f;

    void Start() {
        collidersInTriggers = new List<Collider2D>();
    }

    public void JumpUp() {
        collidersInTriggers.ForEach(delegate(Collider2D other) {
            Rigidbody2D rigidbody = other.attachedRigidbody;

            //Debug.Log(rigidbody);

            if(rigidbody != null) {
                rigidbody.AddForce(Vector2.up * jumpForceIndex);

            }
        });

        ScenePageManager._Instance.EnableNextButton();
    }

    void OnTriggerEnter2D(Collider2D other) {
        if(!collidersInTriggers.Contains(other)) {
            collidersInTriggers.Add(other);
        }
    }

    void OnTriggerExit2D(Collider2D other) {
        if(collidersInTriggers.Contains(other)) {
            collidersInTriggers.Remove(other);
        }
    }

    public void OnPointerUp(PointerEventData eventData) {
        hold = false;
    }

    public void OnPointerDown(PointerEventData eventData) {
        hold = true;
        StartCoroutine(FixedCheck());
        referencePosition = transformToTrack.position;
    }

    private IEnumerator FixedCheck() {
        do {
            yield return new WaitForSeconds(0.025f);
            Vector2 deltatracking = referencePosition - ScreenToWorldPointPerspective();

            //Debug.Log(deltatracking);

            if(deltatracking.y > LOWERBUFFER) {
                //Debug.Log("something");
                JumpUp();
                break;
            }

            if(deltatracking.x > LOWERBUFFER) {
                transformToTrack.position += new Vector3(MOVEINDEX, 0, 0);
                transformToTrack.localScale = new Vector3(-1, 1, 1);
            }
            else if(deltatracking.x < -LOWERBUFFER) {
                transformToTrack.position += new Vector3(-MOVEINDEX, 0, 0);
                transformToTrack.localScale = new Vector3(1, 1, 1);
            }

            referencePosition = transformToTrack.position;
        } while(hold);
    }

    public Vector2 ScreenToWorldPointPerspective() {
        Vector3 temp = new Vector3(
            Input.mousePosition.x,
            Input.mousePosition.y,
            Camera.main.nearClipPlane
        );

        Vector3 temp2 = Camera.main.WorldToScreenPoint(referencePosition);

        return temp2 - temp;
    }
}
