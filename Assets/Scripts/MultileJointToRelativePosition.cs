﻿using UnityEngine;
using System.Collections;

public class MultileJointToRelativePosition : MonoBehaviour {

    private AnchoredJoint2D[] jointsToAnchor;
    private Vector2 originalTransform;
    private Transform connectedTransform;

    void Start() {
        connectedTransform = transform.parent.transform;
        originalTransform = connectedTransform.position;
        jointsToAnchor = GetComponents<AnchoredJoint2D>();
    }

    void Update() {
        foreach(AnchoredJoint2D anch in jointsToAnchor) {
            Vector2 v = anch.connectedAnchor;

            v = v + ((Vector2)(connectedTransform.position) - originalTransform);

            anch.connectedAnchor = v;
        }

        originalTransform = connectedTransform.position;
    }
}
