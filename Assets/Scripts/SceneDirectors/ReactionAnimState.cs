﻿using UnityEngine;
using System.Collections;

public class ReactionAnimState : StateMachineBehaviour {

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        InputManager._Instance.mode = InputManager.InputMode.Reaction;
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        base.OnStateExit(animator, stateInfo, layerIndex);
        ScenePageManager._Instance.EnableNextButton();
    }
}
