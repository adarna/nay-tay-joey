﻿using UnityEngine;
using System.Collections;

public class EnableNextStateMachine : StateMachineBehaviour {

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        base.OnStateExit(animator, stateInfo, layerIndex);

        ScenePageManager._Instance.EnableNextButton();
    }
}
