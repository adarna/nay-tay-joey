﻿using UnityEngine;
using System.Collections;

public class IndicatorController : MonoBehaviour {

    private static IndicatorController _instance;
    public static IndicatorController _Instance {
        get {
            if(_instance == null) {
                _instance = FindObjectOfType<IndicatorController>();
            }
            return _instance;
        }
    }

    public Animator[] indicatorAnim;
    public float[] indicatorDelay;

    private IEnumerator[] coroutines;

    void Start() {
        coroutines = new IEnumerator[indicatorAnim.Length];
    }

    void Update() {
        if(Input.GetMouseButton(0)) {
            StopAllCoroutines();
        }
        else if(Input.GetMouseButtonUp(0)) {
            for(int i = 0; i < coroutines.Length; i++) {
                if(coroutines[i] != null) {
                    coroutines[i] = DelayedIndicator(i);
                    StartCoroutine(coroutines[i]);
                }
            }
        }
    }

    public void ShowIndicator(int index) {
        //Debug.Log("Coroutine starting. Index - " + index + " Delay - " + indicatorDelay[index]);

        //indicatorAnim[index].SetTrigger("Indicator");
        //StartCoroutine(DelayedIndicator(index));
        coroutines[index] = DelayedIndicator(index);
        StartCoroutine(coroutines[index]);
    }

    private IEnumerator DelayedIndicator(int index) {
        yield return new WaitForSeconds(indicatorDelay[index]);
        indicatorAnim[index].SetTrigger("Indicator");

        coroutines[index] = DelayedIndicator(index);
        StartCoroutine(coroutines[index]);
    }

    public void StopIndicator(int index) {
        StopCoroutine(coroutines[index]);
        coroutines[index] = null;
    }
}
