﻿using UnityEngine;
using System.Collections;

public class DirectorSignal : StateMachineBehaviour {

    public ScenePageManager.DirectorSignal signal;

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        base.OnStateExit(animator, stateInfo, layerIndex);
        ScenePageManager._Instance.SignalDirector(signal);

        //Debug.Log("Object: " + animator.gameObject + " Signal: " + signal);
    }
}
