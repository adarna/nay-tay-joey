﻿using UnityEngine;
using System.Collections;

public class IndicatorStateMachine : StateMachineBehaviour {

    public int index = 0;
    public bool onEntrance = true;
    public bool onExit = true;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        base.OnStateEnter(animator, stateInfo, layerIndex);

        if(onEntrance)
            IndicatorController._Instance.ShowIndicator(index);
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        base.OnStateExit(animator, stateInfo, layerIndex);

        if(onExit)
            IndicatorController._Instance.StopIndicator(index);
    }
}
