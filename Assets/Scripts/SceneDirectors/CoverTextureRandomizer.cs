﻿using UnityEngine;
using System.Collections;

public class CoverTextureRandomizer : MonoBehaviour {

    public MeshRenderer meshRenderer;

    public Material[] materials;

    public void RandomizeTexture() {
        meshRenderer.material = materials[Random.Range(0, materials.Length)];
    }
}
