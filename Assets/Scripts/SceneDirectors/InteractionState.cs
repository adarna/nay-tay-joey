﻿using UnityEngine;
using System.Collections;

public class InteractionState : StateMachineBehaviour {

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        //InputManager._Instance.noInput = false;
        InputManager._Instance.mode = InputManager.InputMode.Interaction;
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        base.OnStateExit(animator, stateInfo, layerIndex);
        //InputManager._Instance.noInput = true;
    }
}
