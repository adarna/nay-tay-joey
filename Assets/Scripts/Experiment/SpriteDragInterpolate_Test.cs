﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class SpriteDragInterpolate_Test : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

    public Transform transformToTrack;

    private bool hold = false;
    private Vector3 referencePosition;
    private const float LOWERBUFFER = 1.5f;

    public void OnPointerUp(PointerEventData eventData) {
        hold = false;
    }

    public void OnPointerDown(PointerEventData eventData) {
        hold = true;
        StartCoroutine(FixedCheck());
        referencePosition = transformToTrack.position;
    }

    private IEnumerator FixedCheck() {
        do {
            yield return new WaitForSeconds(0.05f);

            Vector3 temp = new Vector3(
                Input.mousePosition.x,
                Input.mousePosition.y,
                Camera.main.nearClipPlane
                );

            Vector3 temp2 = Camera.main.WorldToScreenPoint(referencePosition);

            Vector2 deltatracking = temp - temp2;

            Debug.DrawLine(temp, temp2);
            //Debug.Log(deltatracking);

            if(deltatracking.x > LOWERBUFFER) {
                transformToTrack.position += new Vector3(0.1f, 0, 0);
            }
            else if(deltatracking.x < -LOWERBUFFER) {
                transformToTrack.position += new Vector3(-0.1f, 0, 0);
            }

            referencePosition = transformToTrack.position;
        } while(hold);
    }
}
