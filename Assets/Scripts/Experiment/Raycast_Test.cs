﻿using UnityEngine;
using System.Collections;

public class Raycast_Test : MonoBehaviour {

    void Update() {
        if(Input.GetMouseButtonUp(0)) {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit2D[] raycasthit2d = Physics2D.RaycastAll(ray.origin, ray.direction);

            Debug.DrawRay(ray.origin, ray.direction, Color.white, 10000f);
            Debug.Log(raycasthit2d.Length);
        }
    }
}
