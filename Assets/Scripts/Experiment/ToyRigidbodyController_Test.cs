﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ToyRigidbodyController_Test : MonoBehaviour {

    //public Rigidbody2D rigidbody;
    public float jumpForceIndex = 1.0f;

    public List<Collider2D> collidersInTriggers;

    void Start() {
        collidersInTriggers = new List<Collider2D>();
    }

    void Update() {
        if(Input.GetKey(KeyCode.A)) {
            transform.position += new Vector3(-0.1f, 0, 0);
        }
        else if(Input.GetKey(KeyCode.D)) {
            transform.position += new Vector3(0.1f, 0, 0);
        }
        else if(Input.GetKeyDown(KeyCode.Space)) {
            JumpUp();
        }
    }

    public void JumpUp() {
        //for(int i = 0; i < collidersInTriggers.Count; i++) {
        //    collidersInTriggers
        //}
        collidersInTriggers.ForEach(delegate(Collider2D other) {
            Rigidbody2D rigidbody = other.attachedRigidbody;

            //Debug.Log(rigidbody);

            if(rigidbody != null) {
                rigidbody.AddForce(Vector2.up * jumpForceIndex);
                
            }
        });
    }

    void OnTriggerEnter2D(Collider2D other) {
        if(!collidersInTriggers.Contains(other)) {
            collidersInTriggers.Add(other);
        }
    }

    void OnTriggerExit2D(Collider2D other) {
        if(collidersInTriggers.Contains(other)) {
            collidersInTriggers.Remove(other);
        }
    }
}
