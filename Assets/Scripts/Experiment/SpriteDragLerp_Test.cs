﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class SpriteDragLerp_Test : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

    public Transform transformToTrack;

    private bool hold = false;
    private Vector3 referencePosition;
    private const float LOWERBUFFER = .005f;

    public TextMesh debugText;

    public void OnPointerUp(PointerEventData eventData) {
        hold = false;
    }

    public void OnPointerDown(PointerEventData eventData) {
        hold = true;
        StartCoroutine(FixedCheck());
        referencePosition = transformToTrack.position;
    }

    private IEnumerator FixedCheck() {
        do {
            yield return new WaitForSeconds(0.05f);

            Vector3 temp = new Vector3(
                Input.mousePosition.x,
                Input.mousePosition.y,
                Camera.main.nearClipPlane
                );

            Vector3 temp2 = Camera.main.WorldToScreenPoint(referencePosition);

            Vector2 deltatracking = temp - temp2;
            deltatracking *= 0.001f;

            Debug.DrawLine(temp, temp2);
            //Debug.Log(deltatracking);
            //Debug.Log(deltatracking.sqrMagnitude);
            debugText.text = ("Vector: " + deltatracking + "\nDistance: " + deltatracking.sqrMagnitude);

            if(Mathf.Abs(deltatracking.x + deltatracking.y) > LOWERBUFFER) {
                //deltatracking *= 0.1f;
                //transformToTrack.position = Vector3.Lerp(transformToTrack.position, temp2, 0.5f);
                transformToTrack.position += new Vector3(
                    deltatracking.x,
                    deltatracking.y,
                    0
                );
            }

            referencePosition = transformToTrack.position;
        } while(hold);
    }
}
