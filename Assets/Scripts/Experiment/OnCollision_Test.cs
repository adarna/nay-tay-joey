﻿using UnityEngine;
using System.Collections;

public class OnCollision_Test : MonoBehaviour {

    void OnCollisionEnter2D(Collision2D other) {
        Debug.Log("Hit: " + other);
    }

    void OnTriggerEnter2D(Collider2D other) {
        Debug.Log("Hit: " + other);
    }
}
