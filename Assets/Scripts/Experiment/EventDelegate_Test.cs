﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class EventDelegate_Test : MonoBehaviour {

    public UnityEvent startEvent;
    public UnityEvent endEvent;

    public EventDelegate_Test nextEventDelegate;
}
