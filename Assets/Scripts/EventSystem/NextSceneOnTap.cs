﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class NextSceneOnTap : MonoBehaviour, IPointerClickHandler {

    public void OnPointerClick(PointerEventData eventData) {
        ScenePageManager._Instance.NextScene();
    }
}
