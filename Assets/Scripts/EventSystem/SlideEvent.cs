﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;

public class SlideEvent : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler {

    public UnityEvent slideLeftEvent;
    public UnityEvent slideRightEvent;
    public UnityEvent slideUpEvent;
    public UnityEvent slideDownEvent;

    public bool vertical;

    private Vector2 startingPosition;
    private bool sliding = false;
    private SlideDirection slideDirection;

    private const float LOWERSWIPEBUFFER = 115f;

    public enum SlideDirection {
        None, Left, Right, Up, Down
    }

    public void OnPointerDown(PointerEventData eventData) {
        startingPosition = Input.mousePosition;
        sliding = true;
    }

    public void OnPointerUp(PointerEventData eventData) {
        SlideReleased();
    }

    public void OnPointerExit(PointerEventData eventData) {
        SlideReleased();
    }

    private void SlideReleased() {
        sliding = false;
    }

    void Update() {
        if(sliding) {
            slideDirection = CheckSwipeDirection((Vector2)(Input.mousePosition) - startingPosition);

            if(slideDirection == SlideDirection.Left) {
                slideLeftEvent.Invoke();
            }
            else if(slideDirection == SlideDirection.Right) {
                slideRightEvent.Invoke();
            }
            else if(slideDirection == SlideDirection.Up) {
                slideUpEvent.Invoke();
            }
            else if(slideDirection == SlideDirection.Down) {
                slideDownEvent.Invoke();
            }
        }
    }

    public static SlideDirection CheckSwipeDirection(Vector2 positionDifference) {
        if(Mathf.Abs(positionDifference.x) > Mathf.Abs(positionDifference.y)) {
            if(positionDifference.x > LOWERSWIPEBUFFER) {
                return SlideDirection.Right;
            }
            else if(positionDifference.x < -LOWERSWIPEBUFFER) {
                return SlideDirection.Left;
            }
        }
        else {
            if(positionDifference.y > LOWERSWIPEBUFFER) {
                return SlideDirection.Up;
            }
            else if(positionDifference.y < -LOWERSWIPEBUFFER) {
                return SlideDirection.Down;
            }
        }
        return SlideDirection.None;
    }
}
