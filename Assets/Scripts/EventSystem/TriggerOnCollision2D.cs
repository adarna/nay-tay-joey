﻿using UnityEngine;
using System.Collections;

public class TriggerOnCollision2D : MonoBehaviour {

    public Animator anim;
    public string triggerName;

    void OnCollisionEnter2D(Collision2D other) {
        anim.SetTrigger(triggerName);
    }

    void OnTriggerEnter2D(Collider2D other) {
        anim.SetTrigger(triggerName);
    }
}
