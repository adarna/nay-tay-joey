﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class LoadSceneOnExit : StateMachineBehaviour {

    public string sceneToLoad;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        SceneManager.LoadScene(sceneToLoad);
    }
}
