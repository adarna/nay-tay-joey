﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class TriggerOnTargetCollider : MonoBehaviour, IPointerUpHandler, IPointerDownHandler {

    public Animator targetAnim;
    public Collider targetCollider;
    public string triggerName;

    public string boolToReset;

    public void OnPointerUp(PointerEventData eventData) {
        Ray ray;
        RaycastHit hit;
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        Debug.DrawRay(ray.origin, ray.direction, Color.red, 1000000f);

        if(Physics.Raycast(ray, out hit)) {
            Debug.Log(hit.collider);

            if(hit.collider == targetCollider) {
                //Debug.Log("something");
                targetAnim.SetTrigger(triggerName);

                if(!string.IsNullOrEmpty(boolToReset)) {
                    GetComponent<Animator>().SetBool(boolToReset, false);
                }
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData) {
        
    }
}
