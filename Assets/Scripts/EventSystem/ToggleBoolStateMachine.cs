﻿using UnityEngine;
using System.Collections;

public class ToggleBoolStateMachine : StateMachineBehaviour {

    public string boolName;
    public BoolAction mode;

    public enum BoolAction {
        Toggle, False, True
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        base.OnStateExit(animator, stateInfo, layerIndex);

        if(mode == BoolAction.False) {
            animator.SetBool(boolName, false);
        }
        else if(mode == BoolAction.True) {
            animator.SetBool(boolName, true);
        }
        else if(mode == BoolAction.Toggle) {
            animator.SetBool(boolName, !animator.GetBool(boolName));
        }
    }
}
