﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class SwipeTrigger : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler {

    private Vector2 startingPosition;
    private bool swiping = false;
    private SwipeDirection swipeDirection;

    public Animator anim;
    public string triggerName;
    public SwipeDirection targetDirection;

    private const float LOWERSWIPEBUFFER = 115f;
    private const float THROWBUFFER = 20f;

    public enum SwipeDirection {
        None, Left, Right, Up, Down
    }

    void Update() {
        if(swiping) {
            swipeDirection = CheckSwipeDirection((Vector2)(Input.mousePosition) - startingPosition);

            if(swipeDirection == targetDirection) {
                anim.SetTrigger(triggerName);
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData) {
        startingPosition = Input.mousePosition;
        swiping = true;
    }

    public void OnPointerUp(PointerEventData eventData) {
        SwipeReleased();
    }

    public void OnPointerExit(PointerEventData eventData) {
        if(swiping)
            CheckThrowDirection((Vector2)(Input.mousePosition) - startingPosition);
        SwipeReleased();
    }

    private void SwipeReleased() {
        swiping = false;
    }

    public static SwipeDirection CheckSwipeDirection(Vector2 positionDifference) {
        if(Mathf.Abs(positionDifference.x) > Mathf.Abs(positionDifference.y)) {
            if(positionDifference.x > LOWERSWIPEBUFFER) {
                return SwipeDirection.Right;
            }
            else if(positionDifference.x < -LOWERSWIPEBUFFER) {
                return SwipeDirection.Left;
            }
        }
        else {
            if(positionDifference.y > LOWERSWIPEBUFFER) {
                return SwipeDirection.Up;
            }
            else if(positionDifference.y < -LOWERSWIPEBUFFER) {
                return SwipeDirection.Down;
            }
        }
        return SwipeDirection.None;
    }

    public static SwipeDirection CheckThrowDirection(Vector2 positionDifference) {
        if(Mathf.Abs(positionDifference.x) > Mathf.Abs(positionDifference.y)) {
            if(positionDifference.x > THROWBUFFER) {
                return SwipeDirection.Right;
            }
            else if(positionDifference.x < -THROWBUFFER) {
                return SwipeDirection.Left;
            }
        }
        else {
            if(positionDifference.y > THROWBUFFER) {
                return SwipeDirection.Up;
            }
            else if(positionDifference.y < -THROWBUFFER) {
                return SwipeDirection.Down;
            }
        }
        return SwipeDirection.None;
    }
}
