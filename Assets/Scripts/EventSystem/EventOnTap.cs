﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;

public class EventOnTap : MonoBehaviour, IPointerClickHandler {

    public UnityEvent eventToInvoke;

    public void OnPointerClick(PointerEventData eventData) {
        eventToInvoke.Invoke();
    }
}
