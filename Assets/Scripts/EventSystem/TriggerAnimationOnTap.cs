﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class TriggerAnimationOnTap : MonoBehaviour, IPointerClickHandler {

    public Animator anim;
    public string triggerName;

    void Start() {
        if(anim == null) {
            GetComponent<Animator>();
        }

        if(string.IsNullOrEmpty(triggerName)) {
            triggerName = "Tap";
        }
    }

    public void OnPointerClick(PointerEventData eventData) {
        anim.SetTrigger(triggerName);
    }
}
