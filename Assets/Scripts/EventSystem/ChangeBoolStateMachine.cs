﻿using UnityEngine;
using System.Collections;

public class ChangeBoolStateMachine : StateMachineBehaviour {

    public string boolToChange;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        animator.SetBool(boolToChange, true);
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        base.OnStateExit(animator, stateInfo, layerIndex);
        animator.SetBool(boolToChange, false);
    }
}
