﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class OnCollisionTrigger : MonoBehaviour {

    public Animator triggerAnim;
    public string triggerName;

    void OnCollisionEnter2D(Collision2D other) {
        Debug.Log("Collision");
    }

    void OnTriggerEnter2D(Collider2D other) {
        Debug.Log("Trigger");
    }
}
