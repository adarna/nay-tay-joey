﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class SpriteDraggable : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

    private Vector3 startingPosition;

    public void OnBeginDrag(PointerEventData eventData) {
        startingPosition = gameObject.transform.position;
    }

    public void OnDrag(PointerEventData eventData) {
        gameObject.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(
                Input.mousePosition.x,
                Input.mousePosition.y,
                Camera.main.nearClipPlane + 10)
            );
    }

    public void OnEndDrag(PointerEventData eventData) {
        gameObject.transform.position = startingPosition;
    }
}
