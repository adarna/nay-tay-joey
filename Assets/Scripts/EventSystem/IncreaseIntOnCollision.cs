﻿using UnityEngine;
using System.Collections;

public class IncreaseIntOnCollision : MonoBehaviour {

    public Animator anim;
    public string intName;

    void OnCollisionEnter2D(Collision2D other) {
        anim.SetInteger(intName, anim.GetInteger(intName) + 1);
    }

    void OnTriggerEnter2D(Collider2D other) {
        anim.SetInteger(intName, anim.GetInteger(intName) + 1);
    }
}
