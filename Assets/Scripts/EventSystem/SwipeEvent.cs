﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;

public class SwipeEvent : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler {

    public UnityEvent swipeLeftEvent;
    public UnityEvent swipeRightEvent;
    public UnityEvent swipeUpEvent;
    public UnityEvent swipeDownEvent;

    private Vector2 startingPosition;
    private bool swiping = false;
    private SwipeDirection swipeDirection;

    public float LOWERSWIPEBUFFER = 115f;
    public float THROWBUFFER = 20f;

    public Animator checkAnim;
    public string boolName;

    public enum SwipeDirection {
        None, Left, Right, Up, Down
    }

    public void OnPointerDown(PointerEventData eventData) {
        startingPosition = Input.mousePosition;
        swiping = true;
    }

    public void OnPointerUp(PointerEventData eventData) {
        SwipeReleased();
    }

    public void OnPointerExit(PointerEventData eventData) {
        if(swiping)
            CheckThrowDirection((Vector2)(Input.mousePosition) - startingPosition);
        SwipeReleased();
    }

    private void SwipeReleased() {
        swiping = false;
    }

    void Update() {
        if(checkAnim != null)
            if(!checkAnim.GetBool(boolName))
                return;

        if(swiping) {
            swipeDirection = CheckSwipeDirection((Vector2)(Input.mousePosition) - startingPosition);

            if(swipeDirection == SwipeDirection.Left) {
                swipeLeftEvent.Invoke();
            }
            else if(swipeDirection == SwipeDirection.Right) {
                swipeRightEvent.Invoke();
            }
            else if(swipeDirection == SwipeDirection.Up) {
                swipeUpEvent.Invoke();
            }
            else if(swipeDirection == SwipeDirection.Down) {
                swipeDownEvent.Invoke();
            }
        }
    }

    public SwipeDirection CheckSwipeDirection(Vector2 positionDifference) {
        if(Mathf.Abs(positionDifference.x) > Mathf.Abs(positionDifference.y)) {
            if(positionDifference.x > LOWERSWIPEBUFFER) {
                return SwipeDirection.Right;
            }
            else if(positionDifference.x < -LOWERSWIPEBUFFER) {
                return SwipeDirection.Left;
            }
        }
        else {
            if(positionDifference.y > LOWERSWIPEBUFFER) {
                return SwipeDirection.Up;
            }
            else if(positionDifference.y < -LOWERSWIPEBUFFER) {
                return SwipeDirection.Down;
            }
        }
        return SwipeDirection.None;
    }

    public SwipeDirection CheckThrowDirection(Vector2 positionDifference) {
        if(Mathf.Abs(positionDifference.x) > Mathf.Abs(positionDifference.y)) {
            if(positionDifference.x > THROWBUFFER) {
                return SwipeDirection.Right;
            }
            else if(positionDifference.x < -THROWBUFFER) {
                return SwipeDirection.Left;
            }
        }
        else {
            if(positionDifference.y > THROWBUFFER) {
                return SwipeDirection.Up;
            }
            else if(positionDifference.y < -THROWBUFFER) {
                return SwipeDirection.Down;
            }
        }
        return SwipeDirection.None;
    }
}
