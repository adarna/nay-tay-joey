﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class IncreaseIntOnTap : MonoBehaviour, IPointerClickHandler {

    public Animator anim;
    public string intName;

    public void OnPointerClick(PointerEventData eventData) {
        anim.SetInteger(intName, anim.GetInteger(intName) + 1);
    }
}
