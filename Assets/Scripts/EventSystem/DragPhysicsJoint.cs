﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class DragPhysicsJoint : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

    private Vector3 startingPosition;
    private bool dragging = false;

    private int hits = 0;

    void Update() {
        if(dragging) {
            gameObject.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(
                Input.mousePosition.x,
                Input.mousePosition.y,
                Camera.main.nearClipPlane + 10)
            );
        }
    }

    public void OnPointerDown(PointerEventData eventData) {
        startingPosition = gameObject.transform.position;
        dragging = true;
    }

    public void OnPointerUp(PointerEventData eventData) {
        gameObject.transform.position = startingPosition;
        dragging = false;
    }

    void OnCollisionEnter2D(Collision2D other) {
        hits++;

        if(hits > 7) {
            GetComponent<Animator>().SetTrigger("Dizzy");
            hits = 0;
        }
    }
}
