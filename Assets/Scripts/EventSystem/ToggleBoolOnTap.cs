﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class ToggleBoolOnTap : MonoBehaviour, IPointerClickHandler {

    public Animator anim;
    public string boolName;
    public BoolAction mode;

    public enum BoolAction {
        Toggle, False, True
    }

    public void OnPointerClick(PointerEventData eventData) {
        if(mode == BoolAction.False) {
            anim.SetBool(boolName, false);
        }
        else if(mode == BoolAction.True) {
            anim.SetBool(boolName, true);
        }
        else if(mode == BoolAction.Toggle) {
            anim.SetBool(boolName, !anim.GetBool(boolName));
        }
    }
}
